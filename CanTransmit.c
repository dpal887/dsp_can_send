/*** VisSim Automatic C Code Generator Version 8.0C15 ***/
/*Fixed and Edited by Daniel Paley*/
/*  Output for C:\Users\OEM\Downloads\Diagram2.vsm at Wed Jan 21 11:03:43 2015 */


#include "math.h"
#include "cgen.h"
#include "c2000.h"
#include "canBus.h"
#include "SendLib.h"
#include "Setup.h"

CAN_TRANSMIT canTrans2 	= 	{ 0x2, 0x5, 8, 0, 1, 0, 0, 0, 0, 0, 0};

extern CGDOUBLE Zed;

static SIM_STATE tSim={0, 0.01, 10,0,0.01,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
SIM_STATE *sim=&tSim;

//Variables to hold the messages. Currently Supporting 4 integer values
int eMesg1=0,eMesg2=0,eMesg3=0,eMesg4=0;

//Interrupt handling send operations
static INTERRUPT void cgMain()
{
	
  send(eMesg1,eMesg2,eMesg3,eMesg4, canTrans2);

}

void delay10ms (unsigned  long time  )
{
    while  (time>0)
    {
        int i = 1333333;   // This number is equivalent to 100 ms
        while (i>0)
        {
            i--;
        }
        time--;
    }
}

void main()
{
  int a = 1;
  
  GenericSetup(canTrans2, tSim);
  
  installInterruptVec(-2,7,&cgMain);
  
  TimerSetup();
  
  //It would be possible to set a while loop here to constantly set different methods
  
  eMesg2 = 0;
  eMesg3 = 0;
  eMesg4 = 0;
  
  while(a == 1){
  	delay10ms(10);
  	eMesg1++;
  	if(eMesg1 == 65){
  		eMesg1 = 0;
  		eMesg2++;
  	}
  	if(eMesg2 == 66){
  		eMesg2 = 0;
  		eMesg3++;
  	}
  	if(eMesg3 == 67){
  		eMesg3 = 0;
  		eMesg4 ++;
  	}
  	if(eMesg4 == 68){
  		eMesg4 = 0;
  	}
  }
  
  dspWaitStandAlone();
}
