#include "math.h"
#include "cgen.h"
#include "c2000.h"
#include "canBus.h"

void send(int b1,int b2,int b3,int b4, CAN_TRANSMIT canTrans2){
	int t3;
	ADCTRL2 |= 0x4040;  // Reset ADC Seq
	ADCTRL2 |= 0x2020;  // Trigger ADC
	t3 = (!(CANTRSB & 0x10L)) /* Check for CAN mailbox busy */;
	if ( t3)
	    	{GPASET = 0x1L;}
	else
	    	{GPACLEAR = 0x1L;};
	if( t3)
	{
	  canTrans2.in1 = (short)(b1);
	  canTrans2.in2 = (short)(b2);
	  canTrans2.in3 = (short)(b3);
	  canTrans2.in4 = (short)(b4);
	  ecan_write_mbox(1, &canTrans2);
	};
	
	sim->tickCount++;
  endOfSampleCount = TIMER2TIM;
}


