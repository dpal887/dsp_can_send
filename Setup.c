#include "math.h"
#include "cgen.h"
#include "c2000.h"
#include "canBus.h"

void GenericSetup(CAN_TRANSMIT canTrans2, SIM_STATE tSim){
	noIntegrationUsed = 1;
  	EALLOW;
  	PLLSTS = 0x10; // reset clk check
  	WDCR=0x00ef;	// Disable Watchdog
  	asm("	clrc DBGM");
  	if (!(PLLSTS&8))	// Skip PLL set if OSC failure
    { PLLSTS = 0x40;	//Disable OSC check
      PLLCR = 0xa;	// set PLL to 5xOSC = 150 MHZ;
      PLLSTS = 0x100;	//Enable OSC check (&F283xx /2 mode)
    }
  	PCLKCR |= 0x8008;
  	HISPCP = 0x0;    // HCLK  = 150 MHZ
  	PCLKCR3 = 0x3400;
  	EDIS;
  	EALLOW;
  	GPAQSEL1 = 0xF330000;
  	GPAQSEL2 = 0xF00000F0;
  	GPAMUX1 = 0x220000;
  	GPADIR = 0x1;
  	GPAPUD = 0xFFFFFAFF;
  	GPBMUX1 = 0x0;
  	GPCMUX1 = 0x0;
  	EDIS;
  	simInit( &tSim );
  	ecan_bus_init(1, 0x2007fL, 0);
  	ecan_bus_trans_init(1, &canTrans2);
  	startSimDsp();
}

void TimerSetup(){

  TIMER2PRD = 0xe360; // 32-bit Timer Period Low
  TIMER2PRDH = 0x16; // 32-bit Timer Period High
  TIMER2TCR |= 0x4020; //Interrupt enable, Timer Reset
  EALLOW;
  PIECTRL = 1; // Enable PIE Interrupts
  EDIS;
  IER |= 0x2000; //CPU Interrupt enable
  resetInterrupts();
  enable_interrupts();  // Global Start Interrupts

}
