/*
// 	Linker Command File For VisSim Code Generator F28335 Target
// +Flash +HotLink +on-chip RAM
*/

C:\Vissim80\cg\lib\f28xxFLASHboot_fpu.obj
C:\Vissim80\cg\lib\initFlash_fpu.obj
C:\Vissim80\cg\lib\DSP2833x_GlobalVariableDefs.obj
-l lib\F280X_fpu.lib
-l lib\SFO_TI_Build_V5B_fpu.lib
-l rts2800_fpu32.lib
-l lib\iqDMC_fpu32.lib
-l lib\iqMATH_fpu32.Lib


MEMORY
{
PAGE 0 :
   PL0L1RAM   : origin = 0x8000, len = 0x6000 /* 0 wait state prog, 0 wait data? */
   PEXTSRAM   : origin = 0x100000, len = 0x40000  /* 1/4 Mword extern static RAM */
   OTPBOOT    : origin = 0x3D7800, len = 0x2      /* on-chip OTP */
   OTP        : origin = 0x3D7802, len = 0x7FE    /* on-chip OTP */
/* FLASH      : origin = 0x3F4000, len = 0x3F80   /* 2801 FLASH  */
/* FLASH      : origin = 0x3F0000, len = 0x7F80   /* 2806 FLASH  */
/* FLASH      : origin = 0x3E8000, len = 0xFF80   /* 2808 FLASH */
   FLASH      : origin = 0x300000, len = 0x3FF80   /* F28335 FLASH */
   FLASHBOOT  : origin = 0x33FFF6, len = 0x2	  /* bootloader JMPs here in Flash mode. */
   ZONE7A     : origin = 0x200000, length = 0x00FC00    /* XINTF zone 7 - program space */ 
   CSM_RSVD   : origin = 0x33FF80, length = 0x000076     /* Part of FLASHA.  Program with all 0x0000 when CSM is in use. */
   CSM_PWL    : origin = 0x33FFF8, length = 0x000008     /* Part of FLASHA.  CSM password locations in FLASHA            */
   ADC_CAL    : origin = 0x380080, length = 0x000009
   PASSWDS    : origin = 0x3F7FF8, len = 0x8
   IQTABLES   : origin = 0x3FE000, length = 0x000b50
   IQTABLES2  : origin = 0x3FEB50, length = 0x00008c
   FPUTABLES  : origin = 0x3FEBDC, length = 0x0006A0
   BOOTROM    : origin = 0x3FF27C, length = 0x000D44               
   RESETINTVEC: origin = 0x3FFFC0, len = 0x2 

PAGE 1 : 
   RAMM0M1    : origin = 0x0,    len = 0x7FC
   MBOX       : origin = 0x7FC,  len = 0x4   
   DRAMH0     : origin = 0x8000, len = 0x8000	/* 0xC000 is 1 waitstate Prog, 0 wait data */
   ZONE7B     : origin = 0x20FC00, length = 0x000400     /* XINTF zone 7 - data space */
   /* Peripheral Frame 0:   */
   DEV_EMU    : origin = 0x000880, length = 0x000180
   FLASH_REGS : origin = 0x000A80, length = 0x000060
   CSM        : origin = 0x000AE0, length = 0x000010
   ADC_MIRROR : origin = 0x000B00, length = 0x000010     /* ADC Results register mirror */
   XINTF      : origin = 0x000B20, length = 0x000020
   CPU_TIMER0 : origin = 0x000C00, length = 0x000008
   CPU_TIMER1 : origin = 0x000C08, length = 0x000008		 
   CPU_TIMER2 : origin = 0x000C10, length = 0x000008		 
   PIE_CTRL   : origin = 0x000CE0, length = 0x000020
   PIE_VECT   : origin = 0x000D00, length = 0x000100
   DMA        : origin = 0x001000, length = 0x000200     /* DMA Rev 0 registers */
   MCBSPA     : origin = 0x005000, length = 0x000040     /* McBSP-A registers */
   MCBSPB     : origin = 0x005040, length = 0x000040     /* McBSP-B registers */
	   /* Peripheral Frame 1:   */
	   ECANA       : origin = 0x006000, length = 0x000040
	   ECANA_LAM   : origin = 0x006040, length = 0x000040     /* eCAN-A local acceptance masks */
	   ECANA_MOTS  : origin = 0x006080, length = 0x000040     /* eCAN-A message object time stamps */
	   ECANA_MOTO  : origin = 0x0060C0, length = 0x000040     /* eCAN-A object time-out registers */
	   ECANA_MBOX  : origin = 0x006100, length = 0x000100     /* eCAN-A mailboxes */
	   ECANB       : origin = 0x006200, length = 0x000040     /* eCAN-B control and status registers */ 
	   ECANB_LAM   : origin = 0x006240, length = 0x000040     /* eCAN-B local acceptance masks */
	   ECANB_MOTS  : origin = 0x006280, length = 0x000040     /* eCAN-B message object time stamps */
	   ECANB_MOTO  : origin = 0x0062C0, length = 0x000040     /* eCAN-B object time-out registers */
	   ECANB_MBOX  : origin = 0x006300, length = 0x000100     /* eCAN-B mailboxes */

	   EPWM1       : origin = 0x006800, length = 0x000022     /* Enhanced PWM 1 registers */
	   EPWM2       : origin = 0x006840, length = 0x000022     /* Enhanced PWM 2 registers */
	   EPWM3       : origin = 0x006880, length = 0x000022     /* Enhanced PWM 3 registers */
	   EPWM4       : origin = 0x0068C0, length = 0x000022     /* Enhanced PWM 4 registers */
	   EPWM5       : origin = 0x006900, length = 0x000022     /* Enhanced PWM 5 registers */
	   EPWM6       : origin = 0x006940, length = 0x000022     /* Enhanced PWM 6 registers */

	   ECAP1       : origin = 0x006A00, length = 0x000020     /* Enhanced Capture 1 registers */
	   ECAP2       : origin = 0x006A20, length = 0x000020     /* Enhanced Capture 2 registers */
	   ECAP3       : origin = 0x006A40, length = 0x000020     /* Enhanced Capture 3 registers */
	   ECAP4       : origin = 0x006A60, length = 0x000020     /* Enhanced Capture 4 registers */         
      ECAP5       : origin = 0x006A80, length = 0x000020     /* Enhanced Capture 5 registers */         
      ECAP6       : origin = 0x006AA0, length = 0x000020     /* Enhanced Capture 6 registers */         
 
	   EQEP1       : origin = 0x006B00, length = 0x000040     /* Enhanced QEP 1 registers */
	   EQEP2       : origin = 0x006B40, length = 0x000040     /* Enhanced QEP 2 registers */   

	   GPIOCTRL    : origin = 0x006F80, length = 0x000040     /* GPIO control registers */
	   GPIODAT     : origin = 0x006FC0, length = 0x000020     /* GPIO data registers */
	   GPIOINT     : origin = 0x006FE0, length = 0x000020     /* GPIO interrupt/LPM registers */

	   /* Peripheral Frame 2:   */
	   SYSTEM     : origin = 0x007010, length = 0x000020
	   SPIA       : origin = 0x007040, length = 0x000010
	   SCIA       : origin = 0x007050, length = 0x000010
	   XINTRUPT   : origin = 0x007070, length = 0x000010
	   GPIOMUX    : origin = 0x0070C0, length = 0x000020
	   ADC        : origin = 0x007100, length = 0x000020
	   SCIB       : origin = 0x007750, length = 0x000010
      SCIC       : origin = 0x007770, length = 0x000010     /* SCI-C registers */
	   SPIB       : origin = 0x007740, length = 0x000010
	   SPIC       : origin = 0x007760, length = 0x000010     /* SPI-C registers */
	   SPID       : origin = 0x007780, length = 0x000010     /* SPI-D registers */
	   I2CA       : origin = 0x007900, length = 0x000040     /* I2C-A registers */
	   /* CSM Password Locations */
	   CSM_PWL    : origin = 0x33FFF8, length = 0x000008
}
 
 
SECTIONS
{
   /* Allocate program areas: */
    passwords       : > PASSWDS,     PAGE = 0
   .flashBoot       : > FLASHBOOT,   PAGE = 0
/* .OTPBoot         : > OTPBOOT,     PAGE = 0, type=DSECT */
   .reset           : > RESETINTVEC, PAGE = 0, type=DSECT  /* Needed for MicroProcessor mode (external RAM at 0x3FFFC0)*/
   reset            : > RESETINTVEC, PAGE = 0, type=DSECT
   .text            : > FLASH,    PAGE = 0 
   .cinit           : > FLASH,    PAGE = 0
   .econst          : > FLASH,    PAGE = 0      
   .switch          : > FLASH,    PAGE = 0      
   IQmath           : > FLASH,    PAGE = 0
   IQmathTables     : > IQTABLES,  PAGE = 0, TYPE = NOLOAD 
   IQmathTables2    : > IQTABLES2, PAGE = 0, TYPE = NOLOAD 
   FPUmathTables    : > FPUTABLES, PAGE = 0, TYPE = NOLOAD 
   ZONE7DATA        : > ZONE7B,    PAGE = 1  
   /* Allocate data areas: */
   .stack           : > RAMM0M1,     PAGE = 1
   .sysmem          : > RAMM0M1,     PAGE = 1
   .ebss            : > DRAMH0,      PAGE = 1

   /*** Ram Function Section ***/
   secureRamFuncs: LOAD = FLASH, PAGE = 0
     RUN = DRAMH0, PAGE = 1
     RUN_START(_secureRamFuncs_runstart),
     LOAD_START(_secureRamFuncs_loadstart),
     LOAD_END(_secureRamFuncs_loadend)

	/*** Peripheral Frame 0 Register Structures ***/
	   DevEmuRegsFile    : > DEV_EMU,     PAGE = 1
	   FlashRegsFile     : > FLASH_REGS,  PAGE = 1
	   CsmRegsFile       : > CSM,         PAGE = 1
	   AdcMirrorFile     : > ADC_MIRROR,  PAGE = 1   
	   CpuTimer0RegsFile : > CPU_TIMER0,  PAGE = 1
	   CpuTimer1RegsFile : > CPU_TIMER1,  PAGE = 1
	   CpuTimer2RegsFile : > CPU_TIMER2,  PAGE = 1  
	   PieCtrlRegsFile   : > PIE_CTRL,    PAGE = 1      

	/*** Peripheral Frame 1 Register Structures ***/
	   SysCtrlRegsFile   : > SYSTEM,      PAGE = 1
	   SpiaRegsFile      : > SPIA,        PAGE = 1
	   SciaRegsFile      : > SCIA,        PAGE = 1
	   XIntruptRegsFile  : > XINTRUPT,    PAGE = 1
	   AdcRegsFile       : > ADC,         PAGE = 1
	   SpibRegsFile      : > SPIB,        PAGE = 1
	   ScibRegsFile      : > SCIB,        PAGE = 1
	   SpicRegsFile      : > SPIC,        PAGE = 1
	   ScicRegsFile      : > SCIC,        PAGE = 1
	   SpidRegsFile      : > SPID,        PAGE = 1
	   I2caRegsFile      : > I2CA,        PAGE = 1

	/*** Peripheral Frame 2 Register Structures ***/
	   ECanaRegsFile     : > ECANA,       PAGE = 1
	   ECanaLAMRegsFile  : > ECANA_LAM    PAGE = 1   
	   ECanaMboxesFile   : > ECANA_MBOX   PAGE = 1
	   ECanaMOTSRegsFile : > ECANA_MOTS   PAGE = 1
	   ECanaMOTORegsFile : > ECANA_MOTO   PAGE = 1
	   ECanbRegsFile     : > ECANB,       PAGE = 1
	   ECanbLAMRegsFile  : > ECANB_LAM    PAGE = 1   
	   ECanbMboxesFile   : > ECANB_MBOX   PAGE = 1
	   ECanbMOTSRegsFile : > ECANB_MOTS   PAGE = 1
	   ECanbMOTORegsFile : > ECANB_MOTO   PAGE = 1
	   EPwm1RegsFile     : > EPWM1        PAGE = 1   
	   EPwm2RegsFile     : > EPWM2        PAGE = 1   
	   EPwm3RegsFile     : > EPWM3        PAGE = 1   
	   EPwm4RegsFile     : > EPWM4        PAGE = 1   
	   EPwm5RegsFile     : > EPWM5        PAGE = 1   
	   EPwm6RegsFile     : > EPWM6        PAGE = 1
	   ECap1RegsFile     : > ECAP1        PAGE = 1   
	   ECap2RegsFile     : > ECAP2        PAGE = 1   
	   ECap3RegsFile     : > ECAP3        PAGE = 1   
	   ECap4RegsFile     : > ECAP4        PAGE = 1
	   ECap5RegsFile     : > ECAP5        PAGE = 1
	   ECap6RegsFile     : > ECAP6        PAGE = 1
	   EQep1RegsFile     : > EQEP1        PAGE = 1   
	   EQep2RegsFile     : > EQEP2        PAGE = 1               
	   GpioCtrlRegsFile  : > GPIOCTRL     PAGE = 1
	   GpioDataRegsFile  : > GPIODAT      PAGE = 1
	   GpioIntRegsFile   : > GPIOINT      PAGE = 1
	   XintfRegsFile     : > XINTF        PAGE = 1
	   McbspaRegsFile    : > MCBSPA       PAGE = 1
	   McbspbRegsFile    : > MCBSPB       PAGE = 1
	   DmaRegsFile       : > DMA          PAGE = 1
	   PieVectTableFile  : > PIE_VECT     PAGE = 1
	/*** Code Security Module Register Structures ***/
	   CsmPwlFile        : > CSM_PWL,     PAGE = 1
}
