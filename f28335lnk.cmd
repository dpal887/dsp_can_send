/*
// Linker Command File For VisSim Code Generator F28335 Target
// +HotLink +on-chip RAM
*/

C:\Vissim80\cg\lib\f28xxFLASHboot_fpu.obj
C:\Vissim80\cg\lib\DSP2833x_GlobalVariableDefs.obj
-l lib\F280X_fpu.lib
-l lib\SFO_TI_Build_V5B_fpu.lib
-l rts2800_fpu32.lib
-l lib\iqDMC_fpu32.lib
-l lib\iqMATH_fpu32.Lib

-l lib\DSP2833x_Headers_nonBIOS.cmd

MEMORY
{
PAGE 0 :
   PL0L1RAM   : origin = 0x8000, len = 0x6000 /* 0 wait state prog, 0 wait data? */
   PEXTSRAM   : origin = 0x100000, len = 0x40000  /* 1/4 Mword extern static RAM */
   OTPBOOT    : origin = 0x3D7800, len = 0x2      /* on-chip OTP */
   OTP        : origin = 0x3D7802, len = 0x7FE    /* on-chip OTP */
/* FLASH      : origin = 0x3F4000, len = 0x3F80   /* 2801 FLASH  */
/* FLASH      : origin = 0x3F0000, len = 0x7F80   /* 2806 FLASH  */
/* FLASH      : origin = 0x3E8000, len = 0xFF80   /* 2808 FLASH */
   FLASH      : origin = 0x300000, len = 0x3FF80   /* F28335 FLASH */
   FLASHCSMZ  : origin = 0x3F7F80, len = 0x76     /* "Zero when using CSM" */
   FLASHBOOT  : origin = 0x3F7FF6, len = 0x2	  /* bootloader JMPs here in Flash mode. */
   ZONE7A     : origin = 0x200000, length = 0x00FC00    /* XINTF zone 7 - program space */ 
   CSM_RSVD   : origin = 0x33FF80, length = 0x000076     /* Part of FLASHA.  Program with all 0x0000 when CSM is in use. */
   CSM_PWL    : origin = 0x33FFF8, length = 0x000008     /* Part of FLASHA.  CSM password locations in FLASHA            */
   ADC_CAL    : origin = 0x380080, length = 0x000009
   PASSWDS    : origin = 0x3F7FF8, len = 0x8
   IQTABLES   : origin = 0x3FE000, length = 0x000b50
   IQTABLES2  : origin = 0x3FEB50, length = 0x00008c
   FPUTABLES  : origin = 0x3FEBDC, length = 0x0006A0
   BOOTROM    : origin = 0x3FF27C, length = 0x000D44               
   RESETINTVEC: origin = 0x3FFFC0, len = 0x2 

PAGE 1 : 
   RAMM0M1    : origin = 0x0,    len = 0x7FC
   MBOX       : origin = 0x7FC,  len = 0x4   
   DRAMH0     : origin = 0xE000, len = 0x2000	/* 0xC000 is 1 waitstate Prog, 0 wait data */
   ZONE7B     : origin = 0x20FC00, length = 0x000400     /* XINTF zone 7 - data space */
}
 
 
SECTIONS
{
   /* Allocate program areas: */
    passwords       : > PASSWDS,     PAGE = 0
   .flashBoot       : > FLASHBOOT,   PAGE = 0, type=DSECT
/*   .H0Boot          : > RAMH0BOOT,   PAGE = 0 */
/* .OTPBoot         : > OTPBOOT,     PAGE = 0, type=DSECT */
   .reset           : > RESETINTVEC, PAGE = 0  type=DSECT/* Needed for MicroProcessor mode (external RAM at 0x3FFFC0)*/
   reset            : > RESETINTVEC, PAGE = 0, type=DSECT
   .text            : > PL0L1RAM,    PAGE = 0 
   .cinit           : > PL0L1RAM,    PAGE = 0
   .econst          : > PL0L1RAM,    PAGE = 0      
   .switch          : > PL0L1RAM,    PAGE = 0      
   IQmath           : > PL0L1RAM,    PAGE = 0
   IQmathTables     : > IQTABLES,  PAGE = 0, TYPE = NOLOAD 
   IQmathTables2    : > IQTABLES2, PAGE = 0, TYPE = NOLOAD 
   FPUmathTables    : > FPUTABLES, PAGE = 0, TYPE = NOLOAD 
   ZONE7DATA        : > ZONE7B,    PAGE = 1  
   /* Allocate data areas: */
   .stack           : > RAMM0M1,     PAGE = 1
   .sysmem          : > RAMM0M1,     PAGE = 1
   .ebss            : > DRAMH0,      PAGE = 1
}
